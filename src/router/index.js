import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import AttendanceSummary from '@/components/AttendanceSummary'
import EmployeeList from '@/components/EmployeeList'
import CreateUser from '@/components/CreateUser'
import EditUser from '@/components/EditUser'
import SeeHistory from '@/components/SeeHistory'
import Login from '@/components/Login'
import Auth from '@okta/okta-vue'

Vue.use(Auth, {
  issuer: 'https://dev-758213.oktapreview.com/oauth2/default',
  client_id: '0oae8r3s70JcwjR8R0h7',
  redirect_uri: 'http://localhost:8080/implicit/callback',
  scope: 'openid profile email'
})

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/implicit/callback',
      component: Auth.handleCallback()
    },
    {
      path: '/AttendanceSummary',
      name: 'AttendanceSummary',
      component: AttendanceSummary,
      meta: {
        // requiresAuth: true
      }
    },
    {
      path: '/EmployeeList',
      name: 'EmployeeList',
      component: EmployeeList,
      meta: {
        // requiresAuth: true
      }
    },
    {
      path: '/CreateUser',
      name: 'CreateUser',
      component: CreateUser,
      meta: {
        // requiresAuth: true
      }
    },
    {
      path: '/EditUser/:id',
      name: 'EditUser',
      component: EditUser,
      meta: {
        // requiresAuth: true
      }
    },
    {
      path: '/SeeHistory/:id',
      name: 'SeeHistory',
      component: SeeHistory,
      meta: {
        // requiresAuth: true
      }
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login,
      meta: {
        // requiresAuth: true
      }
    }
  ]
})

// router.beforeEach(Vue.prototype.$auth.authRedirectGuard())

export default router